# @summary
#    Test Users Module
class users (
  String $test_user_name,
  String $root_home_dir,
  String $test_user_password,
) {
  user { $users::test_user_name:
    ensure   => present,
    home     => "${users::root_home_dir}${users::test_user_name}",
    password => $users::test_user_password,
  }
}
include 'users'
